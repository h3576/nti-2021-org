n = int(input())

decoration = list(map(int, input().split()))
preparation = list(map(int, input().split()))

time = preparation[0]
left = 0

for i in range(n - 1):
    time += decoration[i]
    if time < preparation[i + 1]:
        left += preparation[i + 1] - time
        time = preparation[i+1]

print(preparation[0] + left)