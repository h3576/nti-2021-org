n, m, k = map(int, input().split())

ans = (m - n + k - 2) // (k - 1)

if ans > 0:
    print(ans)

else:
    print(0)