n = int(input())
destinations = input()
distance = list(map(int, input().split()))

total = 0
totalB1 = 0
totalB2 = 0
typeB1 = 0
typeB2 = 0
typeB1Cash = 0
typeB2Cash = 0

for i in range(n):
    if destinations[i] == 'A':
        total += distance[i]*2

for i in range(n):
    if destinations[i] == 'B':
        typeB1 += 1
        if typeB1 % 2 == 1:
            typeB1Cash = distance[i]*2
        else:
            totalB1 += distance[i]*2
            typeB1Cash = 0

totalB1 += typeB1Cash*2

for i in range(n - 1, -1, -1):
    if destinations[i] == 'B':
        typeB2 += 1
        if typeB2 % 2 == 1:
            typeB2Cash = distance[i]*2
        else:
            totalB2 += typeB2Cash
            typeB2Cash = 0

totalB2 += typeB2Cash

#print(totalB1, totalB2)

total += min(totalB1, totalB2)

print(total)