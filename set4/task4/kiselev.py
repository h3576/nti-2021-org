import matplotlib.pyplot as plt

def get_grid_level(image):
    values = []
    for row in image:
        values.extend(row)
    levels = list(set(sorted(values)))
    diff = []
    for i in range(1, len(levels)):
        diff.append(levels[i] - levels[i-1])
    index = diff.index(max(diff))
    print(levels)
    print(diff)
    print(levels[index])
    return levels[index]

def binarize_image(image):
    level = get_grid_level(image)
    for row in image:
        for i in range(len(row)):
            if row[i] > level:
                row[i] = 1
            else:
                row[i] = 0


n, m = map(int, input().split())
data = []
for i in range(n):
    data.append(list(map(int, input().split())))

binarize_image(data)
plt.imshow(data)
plt.show()
