n, m = map(int, input().split())

info = []

for i in range(0, n):
    row = list(map(int, input().split()))
    info.append(row)

counter = 1

circles = []
circles2 = []
ignore = []

for i in range(0, n):
    for j in range(0, m):
        if info[i][j] == 1:
            counter += 1
            #if counter >= 9:
             #   break
            info[i][j] = counter
        if info[i][j] > 1:
            use = info[i][j]

            if j < m - 1:
                if info[i][j+1] > 1 and info[i][j+1] != use:
                    #print("ign", use)
                    ignore.append(use)

            if i < n - 1:
                if info[i+1][j] == 1:
                    info[i+1][j] = use
            if j < m - 1:
                if info[i][j + 1] == 1:
                    info[i][j+1] = use
            if i < n - 1 and j != 0:
                if info[i + 1][j - 1] == 1:
                    if use not in circles:
                        circles.append(use)
                    info[i + 1][j - 1] = use

for item in circles:
    if item not in ignore:
        circles2.append(item)
    else:
        counter -= 1

print(counter - 1 - len(circles2), len(circles2))