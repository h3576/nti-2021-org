from math import sqrt


class Figure:
    def __init__(self, *args):
        if len(args[0]) == 3:
            self.type = "circle"
            self.xc = args[0][0]
            self.yc = args[0][1]
            self.rc = args[0][2]
        elif len(args[0]) == 6:
            self.type = "triangle"
            self.x1 = args[0][0]
            self.y1 = args[0][1]
            self.x2 = args[0][2]
            self.y2 = args[0][3]
            self.x3 = args[0][4]
            self.y3 = args[0][5]
        elif len(args[0]) == 4:
            self.type = "rectangle"
            self.xr = args[0][0]
            self.yr = args[0][1]
            self.hr = args[0][2]
            self.wr = args[0][3]

    @staticmethod
    def triangleSquare(a, b, c):
        p = (a + b + c) / 2
        s = sqrt(p * (p - a) * (p - b) * (p - c))
        return s

    def isInsideCircle(self, x, y):
        dx = self.xc - x
        dy = self.yc - y
        if sqrt(dx ** 2 + dy ** 2) <= self.rc:
            return True
        return False

    def isInsideRectangle(self, x, y):
        y1 = self.yr + self.hr
        x1 = self.xr
        y2 = self.yr
        x2 = self.xr + self.wr
        if x1 <= x <= x2 and y1 <= y <= y2:
            return True
        return False

    def isInsideTriangle(self, x, y):
        AB = sqrt((self.x1 - self.x2) ** 2 + (self.y1 - self.y2) ** 2)
        BC = sqrt((self.x3 - self.x2) ** 2 + (self.y3 - self.y2) ** 2)
        AC = sqrt((self.x1 - self.x3) ** 2 + (self.y1 - self.y3) ** 2)

        AP = sqrt((self.x1 - x) ** 2 + (self.y1 - y) ** 2)
        BP = sqrt((x - self.x2) ** 2 + (y - self.y2) ** 2)
        CP = sqrt((x - self.x3) ** 2 + (y - self.y3) ** 2)

        sq = self.triangleSquare(AB, BC, AC)
        sq1 = self.triangleSquare(AB, AP, BP)
        sq2 = self.triangleSquare(BC, BP, CP)
        sq3 = self.triangleSquare(AC, AP, CP)

        if sq1 + sq2 + sq3 > sq:
            return False
        return True

    def isInside(self, x, y):
        if self.type == "circle":
            return self.isInsideCircle(x, y)
        if self.type == "rectangle":
            return self.isInsideRectangle(x, y)
        if self.type == "triangle":
            return self.isInsideTriangle(x, y)


class Point:
    def __init__(self, *args):
        #print(len(args[0]))
        self.x = args[0][0]
        self.y = args[0][1]
        self.dx = args[0][2]
        self.dy = args[0][3]
        self.s = args[0][4]

    def movePoint(self, minx, miny, maxx, maxy):
        while self.s > 0:
            if self.x >= maxx or self.x <= minx:
                self.dx *= -1
            if self.y >= maxy or self.y <= miny:
                self.dy *= -1
            self.x += self.dx
            self.y += self.dy
            self.s -= 1


minx, maxx, miny, maxy = map(int, input().split())
n, m = map(int, input().split())
figures = []
total = 0

for i in range(n):
    input_data = list(map(float, input().split()))
    figures.append(Figure(input_data))

for i in range(m):
    input_data = list(map(float, input().split()))
    #print(input_data)
    point = Point(input_data)
    point.movePoint(minx, maxx, miny, maxy)
    for figure in figures:
        if figure.isInside(point.x, point.y):
            total += 1
            break

print(total)