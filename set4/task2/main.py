n, m = map(int, input().split())

info = []
mask = [[0 for j in range(m)] for i in range(n)]

for i in range(0, n):
    row = list(map(float, input().split()))
    info.append(row)

cor = []

for i in range(1, n - 1):
    for j in range(1, m - 1):
        if info[i][j] > info[i - 1][j] and info[i][j] > info[i][j - 1] and info[i][j] > info[i + 1][j] and info[i][j] > \
                info[i][j + 1]:
            cor.append([i, j])

sorted(cor)

for cords in cor:
    print(cords[0], cords[1])
