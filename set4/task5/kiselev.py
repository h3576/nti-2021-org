import math
def _deg(y, x):
    angle = math.degrees(math.atan2(y, x))
    if angle < 0:
        angle = 360 + angle
    return int(angle)

def calc_deg(xc, yc, points):
    degs = []
    for p in points:
        x, y = p
        xt = xc - x
        yt = yc - y
        degs.append(_deg(xt, yt))
    return sorted(degs)

xc, yc = map(int, input().split())
n = int(input())
points = []
for _ in range(n):
    points.append(list(map(int, input().split())))

for deg in calc_deg(xc, yc, points):
    print(deg)
