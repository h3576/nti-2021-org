import math

x, y = map(int, input().split())
n = int(input())

angles = []

for i in range(n):
    xi, yi = map(int, input().split())
    angle = math.degrees(math.atan2(x - xi, y - yi))
    if angle < 0:
        angle += 360
    #print(angle, xi - x, yi - y, x, y, xi, yi)
    angles.append(int(angle))

angles.sort()

for angle in angles:
    print(angle)