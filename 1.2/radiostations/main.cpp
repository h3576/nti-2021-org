#include<iostream>
#include<vector>

using namespace std;

struct Coords{
    int x;
    int y;
};

int main(){
    vector<Coords> points;
    vector<vector<Coords> > luna;

    int N, K, R, L;
    cin >>N >>K >>R >>L;

    for (int i = 0; i < N; i++){
        int x, y;
        cin >>x >>y;
        Coords coord;
        coord.x = x;
        coord.y = y;
        points.push_back(coord);
    }

    for (int i = 0; i < points.size(); i++){
        bool F = false;
        for (int j = 0; j < luna.size(); j++){
            for (int o = 0; o < luna[j].size(); o++){
                int x1 = luna[j][o].x;
                int y1 = luna[j][o].y;
                int x = points[i].x;
                int y = points[i].y;
                if ((x-x1)*(x-x1) + (y-y1)*(y-y1) <= L*L){
                    Coords coord;
                    coord.x = x;
                    coord.y = y;
                    luna[j].push_back(coord);
                    F = true;
                    break;
                }
            }
            if (F){
                break;
            }
        }
        if (!F){
            Coords coord;
            coord.x = points[i].x;
            coord.y = points[i].y;
            vector<Coords> temp;
            temp.push_back(coord);
            luna.push_back(temp);
        }
    }

    for (int i = 0; i < luna.size(); i++){
        for (int j = 0; j < luna[i].size(); j++){
            bool F = false;
            for (int o = 0; o < luna[i].size(); o++){
                int x = luna[i][j].x;
                int y = luna[i][j].y;
                int x1 = luna[i][o].x;
                int y1 = luna[i][o].y;
                if (!((x-x1)*(x-x1) + (y-y1)*(y-y1) <= R*R)){
                    F = true;
                    break;
                }
            }
            if (!F){
                cout <<luna[i][j].x <<" " <<luna[i][j].y <<endl;
                break;
            }
        }
    }

}
