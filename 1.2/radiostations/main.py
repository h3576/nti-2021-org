points = []
luna = []

N, K, R, L = map(int, input().split(' '))

for i in range(N):
    x, y = map(int, input().split(' '))
    points.append((x, y))


for (x, y) in points:
    F = False
    #print("lol")
    for i in range(len(luna)):
        for j in range(len(luna[i])):
            x1, y1 = luna[i][j]
            if (x-x1)**2 + (y-y1)**2 <= L**2:
                luna[i].append((x, y))
                F = True
                #print("BREAK")
                break
        if F:
            break
    if not F:
        luna.append([(x, y)])

for i in range(len(luna)):
    for (x, y) in luna[i]:
        F = False
        for (x1, y1) in luna[i]:
            if not ((x1-x)**2 + (y1-y)**2 <= R**2):
                F = True
                break
        if not F:
            print(x, y)
            break