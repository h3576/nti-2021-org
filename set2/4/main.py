k = int(input())

info_string = input()
info_list = []
bool_list = [False for i in range(len(info_string))]

for c in info_string:
    info_list.append(int(c))

last_c = info_list[0]
beg = 0
subStr = [last_c]
#print(type(subStr))

for i in range(len(info_list)):
    if last_c == info_list[i]:
        subStr.append(info_list[i])
    else:
        if len(subStr) >= k:
            for j in range(beg, i):
                bool_list[j] = True
        beg = i
        subStr = [info_list[i]]
        last_c = info_list[i]

#print(bool_list)

subStr = []
beg = 0

for i in range(len(info_list)):
    if bool_list[i]:
        subStr = []
        continue

    if len(subStr) == 0:
        beg = i
    subStr.append(info_list[i])

    if len(info_list) - 1 == i and not bool_list[i]:
        to_use = info_list[beg - 1]
        for j in range(beg, i + 1):
            info_list[j] = to_use
        #for c in info_list:
            #print(c, end='')
        #print('way1')
        subStr = []
        continue

    if bool_list[i + 1]:
        if beg == 0:
            to_use = 0
            if beg == 0:
                to_use = info_list[i + 1]
            for j in range(beg, i + 1):
                info_list[j] = to_use
            #for c in info_list:
                #print(c, end='')
            #print('way2')
            subStr = []
            continue

        if info_list[beg - 1] == info_list[i + 1]:
            to_use = info_list[beg - 1]
            for j in range(beg, i + 1):
                info_list[j] = to_use
            #for c in info_list:
                #print(c, end='')
            #print('way3')
            #print(beg, i)

        sum_list = [info_list[beg]]
        for j in range(1, i - beg + 1):
            sum_list.append(sum_list[j - 1] + info_list[beg + j])
        #print(sum_list)



        if info_list[beg-1] == 0 and info_list[i + 1] == 1:
            min_sum = 999999999
            min_pos = 0
            for j in range(len(sum_list)):
                if sum_list[j] + (len(sum_list) - j - 1 - (sum_list[-1] - sum_list[j])) < min_sum:
                    min_sum = len(sum_list) - j - 1 - (sum_list[-1] - sum_list[j])
                    min_pos = j
            for j in range(beg, beg + min_pos):
                info_list[j] = 0
            for j in range(beg + min_pos, i + 1):
                info_list[j] = 1
            #for c in info_list:
            #    print(c, end='')
            #print('way4')
            #print(beg, i)
            continue

        if info_list[beg-1] == 1 and info_list[i + 1] == 0:
            if i == len(info_list) - 1:
                continue
            min_sum = 999999999
            min_pos = 0
            for j in range(len(sum_list)):
                if j - sum_list[j] + 1 + len(sum_list) - j - (sum_list[-1] - sum_list[j]) < min_sum:
                    min_sum = j - sum_list[j] + 1 + len(sum_list) - j - (sum_list[-1] - sum_list[j])
                    min_pos = j
            for j in range(beg, beg + min_pos):
                info_list[j] = 1
            for j in range(beg + min_pos, i + 1):
                info_list[j] = 0
            #for c in info_list:
            #    print(c, end='')
            #print('way5')
            #print(beg, i)
            continue

for c in info_list:
    print(c, end='')
print()
