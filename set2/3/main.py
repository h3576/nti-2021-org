import operator

n, p = input().split()

n, p = int(n), int(p)

sinaps = list(map(int, input().split()))

norm_sinaps = []

for i in range(n):
    sinap_struc = {'num': i + 1, 'value': sinaps[i]}
    norm_sinaps.append(sinap_struc)

norm_sinaps.sort(key=operator.itemgetter('value'))

m = 0
sum = 0

for i in range(n - 1, 0, -1):
    sum += norm_sinaps[i]['value']
    m += 1
    if (sum >= p):
        break

pre_sum = 0
for i in range(n - 1, n - m, -1):
    pre_sum += norm_sinaps[i]['value']

exus = []

for i in range(0, n - m):
    if pre_sum + norm_sinaps[i]['value'] >= p:
        break
    exus.append(norm_sinaps[i]['num'])

exus.sort()

print(m)
print(len(exus))
for ex in exus:
    print(ex, end=' ')