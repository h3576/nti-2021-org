#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

int main(){
    int n, maxhr, dur;
    vector<int> rates;

    cin >>n >>maxhr >>dur;
    for (int i = 0; i < n; i++){
        int a;
        cin >>a;
        rates.push_back(a);
    }

    int sum = 0, num = 0, ln = 0;

    for (int i = 0; i < n; i++){
        int rate = rates[i];
        if (rate >= round(maxhr*0.8)){
            ln += 1;
        }
        else{
            if (ln > 0){
                if (ln >= dur){
                    num += 1;
                    sum += ln;
                }
                ln = 0;
            }
        }
    }

    if (ln > 0){
        if (ln >= dur){
            num += 1;
            sum += ln;
            ln = 0;
        }
    }

    cout <<num <<" " <<sum;
}
