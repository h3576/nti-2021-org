n, m = input().split()
n, m = int(n), int(m)
nums = list(map(int, input().split()))

poses = []

type = ''

# if nums[0] > nums[1]:
#     type = 'max'
#     poses.append(0)
# elif nums[0] < nums[1]:
#     type = 'min'
#     poses.append(0)
#
# print(type)

for i in range(1, n - 1):
    if type == '':
        if nums[i] > nums[i - 1] and nums[i] > nums[i + 1]:
            type = 'max'
            poses.append(i)
        elif nums[i] < nums[i - 1] and nums[i] < nums[i + 1]:
            type = 'min'
            poses.append(i)
        continue

    if len(poses) == 1:
        if type == 'min':
            if nums[0] < nums[poses[0]]:
                poses[0] = 0
                continue
        if type == 'max':
            if nums[0] > nums[poses[0]]:
                poses[0] = 0
                continue

    if nums[i] > nums[i - 1] and nums[i] > nums[i + 1]:
        if type == 'min' and abs(nums[i] - nums[poses[-1]]) >= m:
            type = 'max'
            poses.append(i)
        elif type == 'max' and nums[i] >= nums[poses[-1]]:
            poses[-1] = i
        continue
    if nums[i] < nums[i - 1] and nums[i] < nums[i + 1]:
        if type == 'max' and abs(nums[i] - nums[poses[-1]]) >= m:
            type = 'min'
            poses.append(i)
        elif type == 'min' and nums[i] <= nums[poses[-1]]:
            poses[-1] = i
        continue

# if nums[-1] > nums[-2] and type == 'max':
#     poses[-1] = len(nums) - 1
# elif nums[-1] > nums[-2] and type == 'min' and abs(nums[-1] - nums[poses[-1]]) >= m:
#     poses.append(len(nums) - 1)
# elif nums[-1] < nums[-2] and type == 'min':
#     poses[-1] = len(nums) - 1
# elif nums[-1] < nums[-2] and type == 'max' and abs(nums[-1] - nums[poses[-1]]) >= m:
#     poses[-1].append(len(nums) - 1)


print(len(poses))

for pos in poses:
    print(pos + 1, end=' ')
